# -*- coding: utf-8 -*-
"""
Created on Thu May 30 11:22:25 2019

@author: Tsur
"""
import numpy as np
import scipy.ndimage
import scipy.io as sio
import scipy.stats as sts
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import seaborn as sns



#   helpful functions

def calc_unary(s):
    m = s.max()
    return np.exp(s-m) / (np.exp(s-m).sum())


def calc_epot(s1, s2):
    p = np.zeros(2)
    p[0] = s1
    p[1] = s2
    z = np.sum(p)
    p[0] = p[0] / z
    p[1] = p[1] / z
    return p


def calc_log_pot(s1, s2):
    p = np.zeros(2)
    p[0] = s1
    p[1] = s2
    z = np.sum(p)
    if p[0]!=0 and p[1]!=0:
        p[0] = np.log(p[0]) - np.log(z)
        p[1] = np.log(p[1]) - np.log(z)
    else:
        p[0]=1 if p[1]==0 else 0
        p[1]=1 if p[0]==0 else 0
    return p


def calc_fi(yi,miu,var):
    return np.exp(-((yi-miu)**2)/(2.*var))


def get_psi(beta,x_i,xj):
    return np.exp(-beta*((x_i - xj)**2))


# =============================================================================
# Question 1: Approximate Inference for Image Segmentation
# =============================================================================
#   a.	Perform a binary segmentation using naive mean field approximation.
#   b.	Perform a binary segmentation using loopy belief propagation.
 
def Q1a_MeanField(im,height,width,beta,miu1,var1,miu2,var2):
    
    STOP_CONDITION = 10e-5
    
    Q = Q_init(im,height,width,miu1,miu2,var1,var2)
    unpro = np.ones((height+2,width+2)) # 1 = unprocessed, 0 = processed
    segments = np.ones((height+2,width+2))
    
    # update Q:
    while sum(sum(unpro[1:height+1,1:width+1])) > 0: # while all pixels are processed
        row = np.random.choice(range(1, height+1))
        col = np.random.choice(range(1, width+1))
        
        if unpro[row][col] == 1:
            state1 = calc_Q_xi(im,Q,row,col,beta,miu1,var1,xi_val=-1)
            state2 = calc_Q_xi(im,Q,row,col,beta,miu2,var2,xi_val=1)
            both_states = np.array([state1,state2])
            Q_new = calc_unary(both_states)
            unpro[row][col] = 0
            
            if abs(Q[row][col][0]-Q_new[0])<STOP_CONDITION or abs(Q[row][col][1]-Q_new[1])<STOP_CONDITION: # according to the stop condition
                continue
            
            Q[row][col] = Q_new
            unpro = set_as_unprocessed(unpro, row, col)
        
    # update segmentation based on Q:
    for row in range(1, height+1):
        for col in range(1, width+1):
            p = Q[row][col]
            segments[row][col] = np.choose(np.argmax(p), [-1, 1])
                
    return segments
    

def Q_init(im,height,width,miu1,miu2,var1,var2):
    Q = np.zeros((height+2,width+2,2),dtype=float)
    # Initialize Q with fi function:
    for row in range(1, height+1):
        for col in range(1, width+1):
            state1 = calc_fi(im[row][col],miu1,var1)
            state2 = calc_fi(im[row][col],miu2,var2)
            Q[row][col] = calc_epot(state1,state2)
    return Q


def calc_Q_xi(im,Q,row,col,beta,miu,var,xi_val):
    E_Qj_up = (Q[row-1][col][0] * (-beta*((xi_val - (-1))**2))) + (Q[row-1][col][1] * (-beta*((xi_val - (1))**2)))
    E_Qj_right = (Q[row][col+1][0] * (-beta*((xi_val - (-1))**2))) + (Q[row][col+1][1] * (-beta*((xi_val - (1))**2)))
    E_Qj_down = (Q[row+1][col][0] * (-beta*((xi_val - (-1))**2))) + (Q[row+1][col][1] * (-beta*((xi_val - (1))**2)))
    E_Qj_left = (Q[row][col-1][0] * (-beta*((xi_val - (-1))**2))) + (Q[row][col-1][1] * (-beta*((xi_val - (1))**2)))
    return E_Qj_up + E_Qj_right + E_Qj_down + E_Qj_left
    

def set_as_unprocessed(unpro,row,col): #Q changed so neighbors of Xi -> unprocessed
    unpro[row-1][col] = 1
    unpro[row][col-1] = 1
    unpro[row+1][col] = 1
    unpro[row][col+1] = 1
    return unpro
 
    
# =============================================================================
# Loopy Belief Propagation
# =============================================================================

def Q2b_Loopy(im,height,width,beta,miu1,var1,miu2,var2):

    msg = np.ones((height+2,width+2,5,2), dtype=float)
    segments = np.ones((height+2,width+2))
    converged_pixels = np.zeros((height+2,width+2))
    
    while sum(sum(converged_pixels[1:height+1,1:width+1])) < height*width :
        row = np.random.choice(range(1, height+1))
        col = np.random.choice(range(1, width+1))
        if(converged_pixels[row][col] == 0):
            neighbours = get_neighbors(segments,row,col)
            
            # compute m(x_j) from pixel i to 4 neighbors j :
            for neighbour_j in range(0,4):
                m_xj = np.ones(2)
                m_xj[0] = calc_m(im,row,col,msg,neighbour_j,neighbours,beta,miu1,var1,miu2,var2,xj_val=-1)
                m_xj[1] = calc_m(im,row,col,msg,neighbour_j,neighbours,beta,miu1,var1,miu2,var2,xj_val=1)
                msg[row][col][neighbour_j] = m_xj
            
            # update in-msg and check if pixel is converged:
            m_new_1 = get_in_msg(msg,row,col,-1,x_i_ind=0)
            m_new_2 = get_in_msg(msg,row,col,-1,x_i_ind=1)
            if abs(msg[row][col][4][0] - m_new_1) < 10e-5 or abs(msg[row][col][4][1] - m_new_2) < 10e-5:
                converged_pixels[row][col] = 1
            msg[row][col][4][0] = m_new_1
            msg[row][col][4][1] = m_new_2
                
    # update segmentation based on beliefs:
    for row in range(1, height+1):
        for col in range(1, width+1):
            state1 = calc_fi(im[row][col],miu1,var1) * get_in_msg(msg,row,col,-1,x_i_ind=0)
            state2 = calc_fi(im[row][col],miu2,var2) * get_in_msg(msg,row,col,-1,x_i_ind=1)
            p = calc_log_pot(state1, state2)
            segments[row][col] = np.choose(np.argmax(p), [-1, 1])
                
    return segments         
            

def initialize_Segments(height,width):
    segments = np.ones((height+2,width+2))*-2
    small_grid = np.ones((height+2,width+2))
    segments[1:height+1, 1:width+1] = small_grid
    return segments


def calc_m(im,row,col,msg,neighbour_j,neighbours,beta,miu1,var1,miu2,var2,xj_val):
    fi_1 = calc_fi(im[row][col],miu1,var1)
    fi_2 = calc_fi(im[row][col],miu2,var2)
    psi_1 = get_psi(beta,-1,xj_val)
    psi_2 = get_psi(beta,1,xj_val)
    msg_1 = get_in_msg(msg, row, col, neighbour_j, x_i_ind=0)
    msg_2 = get_in_msg(msg, row, col, neighbour_j, x_i_ind=1)
    return (fi_1 * psi_1 * msg_1) + (fi_2 * psi_2 * msg_2)
    

def get_in_msg(msg, row, col, neighbour_j, x_i_ind):   
    m = 1
    if neighbour_j != 0: # up
        m = m * msg[row-1][col][2][x_i_ind]
    if neighbour_j != 1: # right
        m = m * msg[row][col+1][3][x_i_ind]
    if neighbour_j != 2: # down
        m = m * msg[row+1][col][0][x_i_ind]
    if neighbour_j != 3: # left
        m = m * msg[row][col-1][1][x_i_ind]
    return m
    
  
def get_neighbors(im, row, col):
    up = im[row-1, col]
    down = im[row+1, col]
    right = im[row, col+1]
    left = im[row, col-1]
    return [up, down, right, left]



# =============================================================================
#   Question 2: Sampling
# =============================================================================
#	We want to generate samples from the standard normal distribution but truncated to [−1, 1].
#   Suggest a Rejection-Sampling scheme to generate such samples. 
#   Using experiment, find the rejection rate and then justify it theoretically.
#
#	Let p be the density of the truncated normal distribution over the interval [−1, 1] and f(x) = x+1. 
#   Suggest an importance sampling scheme. Write a program that estimates the bias and variance of the 
#   importance sampling estimates of E[f(X)] as a function of the number of samples. 
#   Plot the results and draw conclusions.

def rejection_sampling(N):
    normDist = sts.norm(0,1)
    norm_trancated_dist = integrate.quad(lambda x: normDist.pdf(x),-1,1)[0]
    M=1.18
    samples = np.zeros(N)
    i = 0
    total_samples = 0
    while i < N-1:
        total_samples = total_samples + 1
        x = np.random.uniform(-1,1)
        u = np.random.uniform(0,1)
        p_x = ((1/np.sqrt(2*np.pi))*np.exp(-0.5*(x**2))) / norm_trancated_dist
        # create samples:
        if u <= p_x/(M*0.5):
            samples[i] = x
            i = i + 1
    return samples,total_samples


# =============================================================================
# Importance Sampling
# =============================================================================
    
def importance_sampling(N):
    normDist = scipy.stats.norm(0,1)
    norm_trancated_dist = integrate.quad(lambda x: normDist.pdf(x),-1,1)[0]
    M=1
    w = np.zeros(N)
    f = np.zeros(N)
    for i in range(0,N):
        x = np.random.uniform(-1,1)
        f[i] = x + 1
        p_x = (normDist.pdf(x)) / norm_trancated_dist
        w[i] = p_x / (M*0.5)


    
    # compute estimators:
    ep = sum(f*w) / float(N)
    s = 0
    for i in range(0,N):
        s = s + ((f[i]*w[i] - ep)**2)
    var = s / float(N)
    return ep -1, var


def main():

#    initial params as defined in the excercise
    beta = 20.0
    miu1 = 147.0
    var1 = 0.5
    miu2 = 150.0
    var2 = 0.5
    
    
    
#    Load the image given
    mdict = sio.loadmat("img.mat")
    im = mdict["img1"]
    height,width= im.shape
    rav_img = np.ravel(im)
    N = len(rav_img)
    
    
    
    grid = np.ones((height+2, width+2))
    grid[1:height+1, 1:width+1] = im
    im = grid
    
    meanfield_ans = Q1a_MeanField(im,height,width,beta,miu1,var1,miu2,var2)
    loopy_ans = Q2b_Loopy(im,height,width,beta,miu1,var1,miu2,var2)
    
#   plots
    
    plt.figure(1)
    plt.title('Given Image')
    plt.imshow(im[1:height+1,1:width+1] ,'gray', interpolation = 'None')
    
    plt.figure(2)
    plt.title('MeanField Approximation')
    plt.imshow(meanfield_ans[1:height+1,1:width+1] ,'gray', interpolation = 'None')
    
    plt.figure(3)
    plt.title('Loopy Belief Propogation')
    plt.imshow(loopy_ans[1:height+1,1:width+1] ,'gray', interpolation = 'None')
    
#   question 2 :
    
    N = 100
    times = 200
    
#   rejection
    summ = 0
    for i in range(0,times):
        samp,total_samp = rejection_sampling(N)
        rate_of_rej = float(total_samp-N)/total_samp
        summ = summ + rate_of_rej
    print("Average rejection rate: ", summ / times)
    plt.figure(5)
    plt.title('The Sampled Distribution')
    plt.xlabel("x"); plt.ylabel("pdf(x)");
    sns.distplot(samp)
    
#   importance 
    bi = np.zeros(times)
    var = np.zeros(times)
    samp = np.zeros(times)
    for N in range(1, times):
        bi[N], var[N] = importance_sampling(N)
        samp[N] = N
    
    plt.figure(4)
    plt.title('Given Image')

    ax = plt.subplot(1, 2, 1)
    ax.set_title('Variance')
    ax.plot(samp, var)
    plt.xlabel("N")
    plt.ylabel("Variance")
    
    ax = plt.subplot(1, 2, 2)

    ax.set_title('Bias')
    ax.plot(samp, bi)
    plt.xlabel("N")
    plt.ylabel("Bias")

          
if __name__ == "__main__":
    main()